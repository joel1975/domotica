import React, {useContext, useEffect, useState} from 'react'
import {Button, Card, Form} from "react-bootstrap";
import './login.css'
import {CurrentWindowSizeContext} from '../../contexts/currentWindowSize'
import useFetch from "../../hooks/useFetch";
import useLocalStorage from "../../hooks/useLocalStorage";
import {Redirect} from  'react-router-dom'
import {CurrentUserContext} from '../../contexts/currentUser'


const Login = () => {

    const {screenSize} = useContext(CurrentWindowSizeContext)
    const url = '/auth/login'
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isSuccessfulSubmit, setIsSuccessfulSubmit] = useState(false)
    const [{isLoading, response: userResponse, error}, doFetchUser] = useFetch(url)
    const [, setToken] = useLocalStorage('token')
    const [, dispatch] = useContext(CurrentUserContext)

    const submitHandler = (event) => {
        event.preventDefault()
        const user = {email, password}

        doFetchUser({
            method: 'POST',
            data: {
                ...user
            }
        })
    }

    useEffect(() => {
        if (!userResponse) {
            return
        }

        setToken(userResponse.access_token)
        setIsSuccessfulSubmit(true)
        dispatch({type: 'SET_AUTHORIZED', payload: email})
    }, [userResponse, setToken, dispatch, email])



    if (isSuccessfulSubmit) {
         return <Redirect to="/floor/1" />

    }


    return (
        <div className="login">
            <div className="backdrop">
                <Card>
                    <Card.Body>
                        <Card.Title>Login: </Card.Title>
                        <Form noValidate
                              onSubmit={submitHandler}>
                            <Form.Group controlId="email">
                                <Form.Label>Email Address:</Form.Label>
                                <Form.Control size={screenSize}
                                              type="email"
                                              placeholder="Enter email"
                                              onChange={e => setEmail(e.target.value)}/>
                            </Form.Group>
                            <Form.Group controlId="password">
                                <Form.Label>Password:</Form.Label>
                                <Form.Control size={screenSize}
                                              type="password"
                                              placeholder="Enter password"
                                              onChange={e=> setPassword(e.target.value)}/>
                            </Form.Group>
                            <div style={{textAlign: 'center'}}>
                                {error && <span style={{color: "red", display:"block"}}>{error.response.data.message}</span>}
                                <Button variant="secondary"
                                        size={screenSize}
                                        type="submit"
                                        disabled={isLoading}>Login</Button>
                            </div>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}

export default Login
