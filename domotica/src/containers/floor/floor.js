import React, {useContext, useEffect, useState,useCallback, Fragment} from 'react';
import useFetch from "../../hooks/useFetch";
import {Container, Row, Col, Form, Spinner, ButtonGroup, Button, Alert} from "react-bootstrap";
import  './floor.css'
import {CurrentWindowSizeContext} from "../../contexts/currentWindowSize";
import FloorList from '../../componenten/floorList/floorList'
import FloorPlan from '../../componenten/floorPlan/floorPlan'
import Modal from '../../componenten/UI/modal/modal'
import RoomSettings from "../roomSettings/roomSettings";
import Backdrop from '../../componenten/UI/backdrop/backdrop'


const Floor = ({history, match}) => {

    const {screenSize, screenWidth} = useContext(CurrentWindowSizeContext)
    const [floorId, setFloorId] = useState(match.params.id)

    // == ophalen van de gevraagde floor uit de database ==
    const [{isLoading, response: floorResponse, error: floorError}, doFetchFloor] = useFetch(`/floors/${floorId}`)
    const [currentFloor, setCurrentFloor] = useState({})

    // == ophalen aantal floors in totaal van de database
    const [{response: floorCountResponse}, doFetchCountFloors] = useFetch('/floors/')
    const [countFloors, setCountFloors] = useState(0)

    // == Ophalen van de "fake" gemeten waarde om de 10 sec van de database ==
    const [{response: allRoomMeasuredResponse, error: allRoomMeasuredError}, doFetchAllRoomMeasured] = useFetch('/roomSettings')
    const [currentMeasuredRooms, setCurrentMeasuredRooms] = useState([])


    // wegschrijven van de gewenste waarde naar de database
    const [{response: allRoomSettingsResponse, error: allRoomSettingsError}, doFetchAllRoomSettings] = useFetch('/roomSettings')
    const [currentSettingsRooms, setCurrentSettingsRooms] = useState([])

    // settings van één enkele room
    const [roomSettings, setRoomSettings] = useState({})


    const [showSettingsModal, setShowSettingsModal] = useState(false)
    const [showError, setShowError] = useState(false)
    const [errorText, setErrorText] = useState('')

    // enkel voor tablet (switch between plan of list)
    const [plan, setPlan] = useState(true)


    let labelName = plan ? 'switch to list' : 'switch to plan'
    let isSmallScreen = screenWidth < 600
    let isMediumScreen = screenWidth >= 600 && screenWidth <= 1024
    let isLargeScreen = screenWidth > 1024


    const floorChangeHandler = (floorNumber) => {
        setFloorId(floorNumber)
        history.push({pathname: `/floor/${floorNumber}`} )
    }

    const roomSettingsHandler = (roomSettingsId) => {

        const allCurrentRooms = [...currentSettingsRooms]
        const room = allCurrentRooms.find( room => room.id === roomSettingsId)
        setRoomSettings(room)
        setShowSettingsModal(true)
    }

    const cancelHandler = () => {
        setShowSettingsModal(false)
    }

    const closeModal = useCallback(() => {
        setShowSettingsModal(false)
    }, [])


    useEffect(() => {
        doFetchFloor()
    }, [doFetchFloor, floorId])



    // eerste maal ophalen van "fake" gemeten waarde uit de database
    useEffect(() => {
        doFetchAllRoomMeasured()
    }, [doFetchAllRoomMeasured])


    // daarna om de 10 seconde
    useEffect(() => {
        const timer =  setTimeout(() => doFetchAllRoomMeasured(), 10000)
        return () => clearTimeout(timer)
    })

    useEffect(() => {
        if (!floorResponse || !allRoomMeasuredResponse) {
            return
        }
        setCurrentFloor(floorResponse)
        setCurrentMeasuredRooms(allRoomMeasuredResponse.slice(floorResponse.roomSettingsStart, floorResponse.roomSettingsEnd))
    }, [floorResponse, allRoomMeasuredResponse])



    // ophalen van de gewenste waarde uit de database
    useEffect(() => {
        doFetchAllRoomSettings()
    },[doFetchAllRoomSettings,showSettingsModal])

    useEffect(() => {
        if (!floorResponse || !allRoomSettingsResponse) {
            return
        }
        setCurrentFloor(floorResponse)
        setCurrentSettingsRooms(allRoomSettingsResponse.slice(floorResponse.roomSettingsStart, floorResponse.roomSettingsEnd))
    },[floorResponse, allRoomSettingsResponse])


    // nakijken op errors
    useEffect(() => {
        if (allRoomMeasuredError || floorError || allRoomSettingsError) {
            setShowError(true)

        setErrorText(allRoomMeasuredError ? allRoomMeasuredError.response.status + ' ' + allRoomMeasuredError.response.statusText :
            ( floorError ? floorError.response.status + ' ' + floorError.response.statusText :
                allRoomSettingsError.response.status + ' ' + allRoomSettingsError.response.statusText))
        }

    },[allRoomMeasuredError, floorError, allRoomSettingsError])


    /*
    FloorCountResponse zou ik met een echte back-end anders aanpakken
    en niet alle floors ophalen en daar de lengte van nemen. Maar een
    Rest api schrijven die enkel een count doorgeeft ( SELECT COUNT() )
    */
    useEffect(() => {
        doFetchCountFloors()
    }, [doFetchCountFloors])


    useEffect(() => {
        if (!floorCountResponse) {
            return
        }
        setCountFloors(floorCountResponse.length)
    },[floorCountResponse])



    let colPlan = null
    if ((isMediumScreen && plan && !isSmallScreen) || isLargeScreen) {

        colPlan = <Col md={12} lg={7}>
            <h3 className="floorTitle">{currentFloor.name} Plan</h3>
            <FloorPlan rooms={currentMeasuredRooms}
                       goToSettingsForRoom={roomSettingsHandler}/>
        </Col>
    }


    let colList = null
    if (isSmallScreen || (isMediumScreen && !plan) || isLargeScreen) {
        colList = <Col md={12} lg={5}>
            <h3 className="floorTitle">{currentFloor.name} Rooms</h3>
            <FloorList  rooms={currentMeasuredRooms}
                        goToSettingsForRoom={roomSettingsHandler}/>
        </Col>
    }

    return (
        <Container fluid>
            <Modal show={showSettingsModal}>
                <RoomSettings
                    settingsRoom={roomSettings}
                    handleCancel={cancelHandler}
                    closeModal={closeModal}
                />
            </Modal>
            <Backdrop show={showSettingsModal}/>
            {isMediumScreen && <Row className="justify-content-center my-3">
                <Form.Check
                    type="switch"
                    label={labelName}
                    id="custom"
                    onChange={() => setPlan(!plan)}/>
            </Row>}
            <Row className="justify-content-md-center">
                {!isLoading && floorResponse && !showError &&  (
                    <Fragment>
                        {colPlan}
                        {colList}
                    </Fragment>)}
                {isLoading && <Spinner className="floorSpinner" size={screenSize} animation="grow" variant="secondary"/>}
                {!isLoading && showError && (
                    <Alert variant="danger" style={{margin: '1em'}}>
                        <Alert.Heading>Oh snap! You got an error! &nbsp;
                            {errorText}</Alert.Heading>
                        <p>Maybe this page moved? Got deleted? Is hiding out in quarantine? Never existed in the first place?</p>
                         <p>   Contact JVE Home Control helpdesk at 0800/14888</p>
                    </Alert>
                )}
            </Row>
            <Row className="justify-content-center my-5">
                {!isLoading && floorResponse && !showError && (
                    <ButtonGroup>
                        {Array(countFloors).fill(0).map((v, index ) => index +1).map( floorNumber => {
                            return <Button variant="outline-secondary"
                                           key={floorNumber}
                                           size={screenSize}
                                           active={floorNumber === +floorId}
                                           onClick={() => floorChangeHandler(floorNumber)}
                                           >{floorNumber}</Button>
                        })}
                    </ButtonGroup>
                )}
            </Row>
        </Container>
    );
};

export default Floor


