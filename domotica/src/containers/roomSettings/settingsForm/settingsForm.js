import React, {Fragment, useContext, useEffect, useState} from 'react';
import {Row, Col, Form, Button} from "react-bootstrap";
import {CurrentWindowSizeContext} from '../../../contexts/currentWindowSize'


const SettingsForm = ({settingsName, settings, cancelModal,
                          handleSaveSettings, title, unit}) => {

    const {screenSize} = useContext(CurrentWindowSizeContext)
    const [defaultValue, setDefaultValue] = useState(0)
    const [slotValue, setSlotValue] = useState(0)
    const [startTime, setStartTime] = useState()
    const [endTime, setEndTime] = useState()
    const [slots, setSlots] = useState([])

    const slotHandler = () => {
        const currentSlots = [...slots]
        const newSlot = {
            "startTime": startTime,
            "endTime": endTime,
            "value": +slotValue
        }
        setSlots([...currentSlots, newSlot])
    }

    const removeHandler = (index) => {
        const currentSlots = [...slots]
        currentSlots.splice(index,1)
        setSlots(currentSlots)
    }

    const saveSettingsHandler = () => {

        const newSettings = {
            ...settings,
            default: +defaultValue,
            slots: slots
        }
        handleSaveSettings(newSettings, settingsName)
    }

    useEffect(() => {
        if (!settings) {
            return
        }
        setSlots(settings.slots)
        setDefaultValue(settings.default)
    },[settings])


    return (
        <Fragment>
            <Row className="justify-content-center my-3">
                <h4 style={{textTransform: 'capitalize'}}>{settingsName} {title?.name}</h4>
            </Row>
            <Row>
                <Form >
                    <Form.Row>
                        <Col>
                            {settingsName !== 'curtains' && <h5>Default Value:</h5>}
                            {settingsName === 'curtains' && <h5>Curtains are</h5>}
                        </Col>
                       <Col>
                           <Form.Group controlId="defaultTemp">
                               {settingsName !== 'curtains' &&  <Form.Label>{defaultValue} {unit}</Form.Label>}
                               {settingsName === 'curtains' && <Form.Label>{+defaultValue === 0 ? 'Open' : 'Closed'}</Form.Label>}
                               <Form.Control type="range"
                                             min={settings?.min} max={settings?.max}
                                             value={defaultValue}
                                             onChange={e => setDefaultValue(e.target.value)}/>
                           </Form.Group>
                       </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <Form.Group controlId="startTime">
                                <Form.Label>Start Time</Form.Label>
                                <Form.Control size={screenSize}
                                              type="time"
                                              onChange={e => setStartTime(e.target.value)}/>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId="endTime">
                                <Form.Label>End Time</Form.Label>
                                <Form.Control size={screenSize}
                                              type="time"

                                              onChange={e => setEndTime(e.target.value)}  />
                            </Form.Group>
                        </Col>
                        <Col className="align-self-lg-center">
                            <Form.Group controlId="slotTemp">
                                {settingsName !== 'curtains' && <Form.Label>{slotValue} {unit} </Form.Label>}
                                {settingsName === 'curtains' && <Form.Label>{+slotValue === 0 ? 'Open' : 'Closed'}</Form.Label>}
                                <Form.Control type="range"
                                              value={slotValue}
                                              min={settings?.min} max={settings?.max}
                                              onChange={e => setSlotValue(e.target.value)}/>
                            </Form.Group>
                        </Col>
                        <Col className="align-self-center">
                            <Button size='sm'
                                    variant={"secondary"}
                                    onClick={slotHandler}>Set</Button>
                        </Col>
                    </Form.Row>
                    {slots.map((slot,i) => {
                        return <Form.Row key={i} className="my-2">
                            {settingsName !== 'curtains' && <Col><strong>{slot.startTime} till {slot.endTime} = {slot.value}{unit}</strong></Col>}
                            {settingsName ==='curtains' && <Col><strong>{slot.value === 0 ? 'Open' : 'Closed'} from {slot.startTime} till {slot.endTime}</strong></Col>}
                            <Col><Button size="sm"
                                         variant="danger"
                                         onClick={() => removeHandler(i)}>X</Button></Col>
                        </Form.Row>
                    })}
                    <Form.Row className="justify-content-around mt-4">
                        <Button size={screenSize} variant="light" onClick={cancelModal}>Cancel</Button>
                        <Button size={screenSize} variant="dark" onClick={saveSettingsHandler}>Save</Button>
                    </Form.Row>
                </Form>
            </Row>
        </Fragment>
    );
};

export default SettingsForm;



