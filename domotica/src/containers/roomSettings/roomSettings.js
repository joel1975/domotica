import React, {useContext, useEffect, useState} from 'react';
import {Container, Row, ToggleButtonGroup, ToggleButton, Alert} from "react-bootstrap";
import {CurrentWindowSizeContext} from '../../contexts/currentWindowSize'
import SettingsForm from './settingsForm/settingsForm'
import useFetch from "../../hooks/useFetch";

const RoomSettings = ({settingsRoom, handleCancel, closeModal}) => {

    const {screenSize} = useContext(CurrentWindowSizeContext)
    const [settingNr, setSettingNr] = useState(1)
    const [{response: editRoomSettingsResponse, error: editRoomSettingsError}, doFetchEditRoomSettings] = useFetch(`/roomSettings/${settingsRoom.id}`)
    const [showError, setShowError] = useState(false)

    const settingsHandler = (val) => setSettingNr(val)

    const saveSettingsHandler = (settings, settingsName) => {

       const roomSettingPerName = {
            [settingsName]: {
                ...settings
            }
        }

        doFetchEditRoomSettings({
            method: 'PUT',
            data: {
                ...settingsRoom,
                ...roomSettingPerName
            }
        })
    }

    useEffect(() => {

        if (!editRoomSettingsResponse) {
            return
        }
        closeModal()
    },[editRoomSettingsResponse,closeModal])

    useEffect(() => {
        if(!editRoomSettingsError) {
            return
        }
        setShowError(true)
    }, [editRoomSettingsError])

    return (
       <Container>
           {!showError && <Row className="justify-content-center">
               <ToggleButtonGroup type="radio" name="options" defaultValue={1} onChange={settingsHandler}>
                   <ToggleButton size={screenSize} variant={"outline-secondary"} value={1}>Temperature</ToggleButton>
                   <ToggleButton size={screenSize} variant={"outline-secondary"} value={2}>Luminosity</ToggleButton>
                   <ToggleButton size={screenSize} variant={"outline-secondary"} value={3}>Music</ToggleButton>
                   <ToggleButton size={screenSize} variant={"outline-secondary"} value={4}>Curtains</ToggleButton>
               </ToggleButtonGroup>
           </Row>}
           {showError  && (
               <Alert variant="danger" onClose={() => setShowError(false)} dismissible>
                   <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                   <p>can't write data to server</p>
                   <p>Contact JVE Home Control at 0800/14888</p>
               </Alert>
           )}
           {settingNr === 1 && !showError  && <SettingsForm
               settingsName="temperature"
               settings={settingsRoom.temperature}
               title={settingsRoom.title}
               cancelModal={handleCancel}
               unit="°C"
               handleSaveSettings={saveSettingsHandler}/>}
           {settingNr === 2 && !showError  && <SettingsForm
               settingsName="luminosity"
               settings={settingsRoom.luminosity}
               title={settingsRoom.title}
               cancelModal={handleCancel}
               unit="Watts"
               handleSaveSettings={saveSettingsHandler}/>}
           {settingNr === 3 && !showError  && <SettingsForm
               settingsName="music"
               settings={settingsRoom.music}
               title={settingsRoom.title}
               cancelModal={handleCancel}
               unit="dB"
               handleSaveSettings={saveSettingsHandler}/>}
           {settingNr === 4 && !showError  && <SettingsForm
               settingsName="curtains"
               settings={settingsRoom.curtains}
               title={settingsRoom.title}
               cancelModal={handleCancel}
               handleSaveSettings={saveSettingsHandler}/>}
       </Container>
    );

};

export default RoomSettings;



// onClose={() => setShow(false)} dismissible
