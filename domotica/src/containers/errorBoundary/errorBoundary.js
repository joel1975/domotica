
import React, {Component} from 'react'
import ErrorMessage from '../../componenten/errorMessage/mainErrorMessage/mainErrorMessage'

class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    render() {
        if (this.state.hasError) {

            return <ErrorMessage />;
        }

        return this.props.children;
    }
}

export default ErrorBoundary
