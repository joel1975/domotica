import React, {createContext, useReducer} from "react";

const initialState = {
    isLoggedIn: false,
    email: ''
}

const reducer = (state, action) => {

    switch(action.type) {
        case 'SET_AUTHORIZED':
            return {
                ...state,
                isLoggedIn: true,
                email: action.payload
            }

        case 'SET_UNAUTHORIZED':
            return {
                ...state,
                isLoggedIn: false,
                email: ''
            }

        default :
            return state
    }
}

export const CurrentUserContext = createContext()

export const CurrentUserProvider = ({children}) => {

    const value = useReducer(reducer, initialState)

    return (
        <CurrentUserContext.Provider value={value}>{children}</CurrentUserContext.Provider>
    )
}
