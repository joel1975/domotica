
import React, {useState, useEffect, createContext} from 'react'

const initialValue = {
    screenWidth: 0,
    screenSize: 'sm'
}

export const CurrentWindowSizeContext = createContext(initialValue)

export const CurrentWindowSizeProvider =  ({children}) => {


    const [screenSize, setScreenSize] = useState(document.documentElement.clientWidth > 480 ? 'lg' : 'sm')
    const [screenWidth, setScreenWidth] = useState(document.documentElement.clientWidth)


    useEffect(() => {

        window.addEventListener('resize', resizeHandler)

        function resizeHandler() {
            setScreenSize(document.documentElement.clientWidth > 480 ? 'lg' : 'sm')
            setScreenWidth(document.documentElement.clientWidth)
        }

        return () => window.removeEventListener('resize', resizeHandler)
    },[])


    return <CurrentWindowSizeContext.Provider value={{screenSize:screenSize, screenWidth: screenWidth }} >
        {children}</CurrentWindowSizeContext.Provider>
}

