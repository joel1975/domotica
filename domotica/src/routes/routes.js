import React from "react";
import {Switch, Route} from 'react-router-dom'
import Login from '../containers/login/Login'
import AuthGuardRoute from './authGuardRoute'
import Floor from '../containers/floor/floor'
import WithHeader from '../hoc/withHeader'

const Routes = () => {

    return (
        <Switch>
            <Route component={Login} path="/" exact />
            <WithHeader>
                <AuthGuardRoute component={Floor} path='/floor/:id' />
                {/*<Route component={Floor} path='/floor/:id'/>*/}
            </WithHeader>
        </Switch>
    )
}

export default Routes
