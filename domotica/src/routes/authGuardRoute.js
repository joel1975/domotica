import React from 'react';
import {Redirect, Route} from 'react-router-dom'
import {CurrentUserContext} from '../contexts/currentUser'
import {useContext} from "react";


const AuthGuardRoute = ({component: Component, ...rest}) => {

    const [{isLoggedIn}] = useContext(CurrentUserContext)



    return (
        <Route {...rest} render={(props) => (
            isLoggedIn ? <Component {...props} /> : <Redirect to='/' />
        )} />
    )
}

export default AuthGuardRoute
