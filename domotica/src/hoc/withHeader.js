import React, {Fragment} from 'react';
import Header from '../componenten/header/header'

const withHeader = ({children}) => {


    return (
        <Fragment>
            <Header/>
            {children}
        </Fragment>
    )
}

export default withHeader
