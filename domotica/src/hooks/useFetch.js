import {useState, useEffect, useCallback} from 'react';
import axios from 'axios'
import useLocalStorage from "./useLocalStorage";

export default url => {

    const baseUrl = 'http://localhost:8000'
    const [isLoading, setIsLoading] = useState(false)
    const [response, setResponse] = useState(null)
    const [error, setError] = useState(null)
    const [options, setOptions] = useState({})
    const [token] = useLocalStorage('token')


    const doFetch = useCallback((options = {}) => {
        setOptions(options)
        setIsLoading(true)
    }, [])



    useEffect(() => {

        if (!isLoading) {
            return
        }

        const requestOptions = {
            ...options,
            ...{
                headers: {
                    Authorization: token ? `Bearer ${token}` : ''
                }
            }
        }

        axios(baseUrl + url, requestOptions)
            .then(response => {
                setResponse(response.data)
                setIsLoading(false)
            })
            .catch(error => {
                setError(error)
                setIsLoading(false)
            })

    }, [isLoading, url, options, token])

    return [{isLoading, response, error}, doFetch]
}
