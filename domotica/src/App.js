import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Routes from './routes/routes'
import {CurrentWindowSizeProvider} from './contexts/currentWindowSize'
import {CurrentUserProvider} from "./contexts/currentUser";
import ErrorBoundary from "./containers/errorBoundary/errorBoundary";


function App() {
  return (
      <ErrorBoundary>
          <CurrentWindowSizeProvider>
              <CurrentUserProvider>
                  <Router>
                      <Routes />
                  </Router>
              </CurrentUserProvider>
          </CurrentWindowSizeProvider>
       </ErrorBoundary>
  );
}

export default App;
