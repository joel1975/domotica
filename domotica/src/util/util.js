

const luminosityToYellow = (value) => {

    let color = ''

    switch (value) {

        case 1 :
        case 2 :
        case 3 :
            color = '#FFFFCC'
            break
        case 4:
        case 5:
        case 6:
            color = '#FFFF99'
            break
        case 7:
        case 8:
        case 9:
            color ='#FFFF66'
            break
        case 10:
        case 11:
        case 12:
            color = '#FFFF33'
            break
        case 13:
        case 14:
            color = '#FFFF00'
            break
        case 15:
        case 16:
            color = '#CCCC00'
            break
        case 17:
        case 18:
        case 19:
        case 20:
            color= '#999900'
            break
        default:
            color = '#999900'

    }
    return color
}


export const  pickRoomColor = (luminosityValue, isCurtainClosed) => {

    let roomColor

    if ((luminosityValue === 0) && (isCurtainClosed === 1)) {
        roomColor= 'black'
    } else if (luminosityValue > 0) {
        roomColor = luminosityToYellow(+luminosityValue)
    } else {
        roomColor = 'white'
    }

    return roomColor
}


export const checkSlotsAndDefault = (room, settingName) => {

    const date= new Date()
    const value = room[settingName]
    const slots = value.slots

    let slotValue = -1

    slots.forEach( slot => {

        const startHour = slot.startTime.split(':')[0]
        const startMins = slot.startTime.split(':')[1]
        const endHour = slot.endTime.split(':')[0]
        const endMins = slot.endTime.split(':')[1]

        const timeStart = new Date()
        timeStart.setHours(startHour)
        timeStart.setMinutes(startMins)

        const timeEnd = new Date()
        timeEnd.setHours(endHour)
        timeEnd.setMinutes(endMins)

        if ((timeStart <= date)  && (date <= timeEnd)) {
            slotValue = slot.value
        }
    })

    if (slotValue >= 0) {
        return slotValue
    } else {
        return value.default
    }
}

