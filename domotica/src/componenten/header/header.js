import React, {useContext} from "react";
import {Jumbotron, Button} from "react-bootstrap";
import classes from './header.module.css'
import {IoMdLogOut} from 'react-icons/io'
import {CurrentWindowSizeContext} from "../../contexts/currentWindowSize";
import {CurrentUserContext} from '../../contexts/currentUser'
import {withRouter} from 'react-router-dom'


const Header = ({history}) => {

    const {screenSize} = useContext(CurrentWindowSizeContext)
    const [, dispatch] = useContext(CurrentUserContext)

    const logoutHandler = () => {
        dispatch({type: 'SET_UNAUTHORIZED'})
        history.push({pathname: '/'})

    }

    return (
        <Jumbotron style={{backgroundColor: '#50c878'}}>
            <div className={classes.Container}>
                <h1 className={classes.Title}>JVE Home Control</h1>
                <Button variant="secondary"
                        size={screenSize}
                        onClick={logoutHandler}><IoMdLogOut/></Button>
            </div>
        </Jumbotron>
    )
}

export default withRouter(Header)
