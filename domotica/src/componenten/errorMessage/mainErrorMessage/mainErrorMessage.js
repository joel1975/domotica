import React from 'react';
import classes from './mainErrorMessage.module.css'

const mainErrorMessage = () => {
    return (
        <div className={classes.ErrorMessage}>
            <h1 className={classes.Title}>Whoops!</h1>
            <p className={classes.Description}>Something went wrong</p>
        </div>
    );
};

export default mainErrorMessage;
