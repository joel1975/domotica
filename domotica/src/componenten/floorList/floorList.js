import React from 'react'
import {ListGroup} from "react-bootstrap";
import classes from './floorList.module.css'
import { pickRoomColor, checkSlotsAndDefault} from "../../util/util";
import {FiMusic} from "react-icons/all";

const listGroup = ({rooms, goToSettingsForRoom}) => {


    return (
        <div>
            <ListGroup className={classes.Group}>
                {rooms.map( room => {

                    // check if there is slot present
                    const tempValue = checkSlotsAndDefault(room, 'temperature')
                    const lumValue = checkSlotsAndDefault(room, 'luminosity')
                    const musicValue = checkSlotsAndDefault(room, 'music')
                    const curtainsValue = checkSlotsAndDefault(room, 'curtains')

                    // pick color for room
                    const roomColor = pickRoomColor(lumValue , curtainsValue);


                    return (
                        <ListGroup.Item variant="secondary"
                                        style={{backgroundColor: roomColor, color: '#A0A0A0'}}
                                        as="div"
                                        className={classes.Item}
                                        onClick={() => goToSettingsForRoom(room.id)}
                                        key={room.id}>{room.title.name}

                            <FiMusic  style={{color: +musicValue === 0 ? 'red' : 'green', marginLeft: '5px'}}/>
                            <div>{tempValue}°C</div>

                        </ListGroup.Item>
                    )
                })}

            </ListGroup>
        </div>
    )
}
export default listGroup


