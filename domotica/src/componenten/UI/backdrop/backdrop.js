import React from 'react';
import classes from './backdrop.module.css'

const backdrop = ({show}) => (show ? <div className={classes.Backdrop}/> : null)

export default backdrop;
