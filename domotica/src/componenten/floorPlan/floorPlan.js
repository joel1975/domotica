import React from 'react';
import {withRouter} from 'react-router-dom'
import classes from './floorPlan.module.css'
import {FiMusic} from "react-icons/all";
import {checkSlotsAndDefault, pickRoomColor} from '../../util/util'


const floorPlan = ({rooms, goToSettingsForRoom}) => {

    const styleCanvas = ({
        outline: '5px solid grey',
        backgroundColor: '#DCDCDC'
    })

    return (
        <div style={{textAlign: 'center'}}>
            <svg width="500" height="500" style={styleCanvas}>

                {rooms.map(room => {

                    // check if there is slot present
                    const tempValue = checkSlotsAndDefault(room, 'temperature')
                    const lumValue = checkSlotsAndDefault(room, 'luminosity')
                    const musicValue = checkSlotsAndDefault(room, 'music')
                    const curtainsValue = checkSlotsAndDefault(room, 'curtains')

                    // pick color for room
                    const roomColor = pickRoomColor(lumValue , curtainsValue);

                    return (
                        <g key={room.id} onClick={() => goToSettingsForRoom(room.id)}>
                            <polygon className={classes.Room}
                                     points={room.points}
                                     fill={roomColor}/>

                            <text  className={classes.Room}
                                   x={room.title.x}
                                   y={room.title.y}
                                   style={{fontSize:'20px'}}
                                   fill="#888888">{room.title.name}
                            </text>

                            <text
                                   x={room.temperature.x}
                                   y={room.temperature.y}
                                   style={{fontSize:'16px'}}
                                   fill="#888888">T: {tempValue}°C
                            </text>

                            <FiMusic className={classes.Sound} style={{color: +musicValue === 0 ? 'red' : 'green'}}
                                                                x={room.music.x}
                                                                y={room.music.y}/>
                        </g>
                    )
                })}
            </svg>
        </div>
    );
};

export default withRouter(floorPlan);




